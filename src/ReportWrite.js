import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import logo from './assets/img/90677.jpg'

export default class ReportWrite extends Component {
    render() {
        return (
            <div className="login-container">
                <ul class="nav login-nav">
                    <li class="nav-item back">
                        <Link class="nav-link" to={"/report-photo"}><i class="fas fa-arrow-left icon"></i>Write Report</Link>
                    </li>
                </ul>
                <div className="write-wrapper container-fluid">
                    <ul class="progressbar">
                        <li>Photo</li>
                        <li class="active">Write</li>
                        <li>Review</li>
                    </ul>
                    <div class="card">
                        <img src={logo} class="card-img-top" alt="..." />
                        <div class="card-body">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Search report label..." />
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-3 active">
                                        <i class="fas fa-trash-alt icon"></i>
                                        <p>Sanitation</p>
                                    </div>
                                    <div class="col-3">
                                        <i class="fas fa-water icon"></i>
                                        <p>Flood</p>
                                    </div>
                                    <div class="col-3">
                                        <i class="fas fa-fire-alt icon"></i>
                                        <p>Fire</p>
                                    </div>
                                    <div class="col-3">
                                        <i class="fas fa-road icon"></i>
                                        <p>Road Damage</p>
                                    </div>
                                    <div class="col-3">
                                        <i class="fas fa-trash-alt icon"></i>
                                        <p>Sanitation</p>
                                    </div>
                                    <div class="col-3">
                                        <i class="fas fa-trash-alt icon"></i>
                                        <p>Sanitation</p>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="alert alert-light" role="alert">
                                <span>#sampahkering</span> <span>#sampahbasah</span>
                            </div>
                            <hr />
                            <div class="form">
                                <textarea class="form-control" placeholder="Write description"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-secondary button-submit">Submit</button>
            </div>
        )
    }
}
