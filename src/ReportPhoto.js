import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class ReportPhoto extends Component {
    render() {
        return (
            <div className="login-container">
                <ul class="nav login-nav">
                    <li class="nav-item back">
                        <Link class="nav-link" to={"/home"}><i class="fas fa-arrow-left icon"></i>Photo Report</Link>
                    </li>
                </ul>
                <div className="photo-wrapper container-fluid">
                    <ul class="progressbar">
                        <li class="active">Photo</li>
                        <li>Write</li>
                        <li>Review</li>
                    </ul>
                    <div class="mt-3">
                        <Link to={"/report-write"}><input class="form-control" type="file" id="formFile" /></Link>
                    </div>
                </div>
            </div>
        )
    }
}
