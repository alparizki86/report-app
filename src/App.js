import React, { Component } from 'react'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import Route from 'react-router-dom/Route'
import Home from './Home'
import Login from './Login'
import ReportPhoto from './ReportPhoto'
import ReportWrite from './ReportWrite'
import Welcome from './Welcome'

export default class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Switch>
            <Route path="/login" exact strict component={Login} />
            <Route path="/" exact strict component={Welcome} />
            <Route path="/home" exact strict component={Home} />
            <Route path="/report-photo" exact strict component={ReportPhoto} />
            <Route path="/report-write" exact strict component={ReportWrite} />
          </Switch>
        </div>
      </Router>
    )
  }
}

