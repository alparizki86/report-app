import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Login extends Component {
    render() {
        return (
            <div className="login-container">
                <ul class="nav login-nav">
                    <li class="nav-item back">
                        <Link class="nav-link" to={"/"}><i class="fas fa-arrow-left icon"></i>Log In</Link>
                    </li>
                    <li class="nav-item next">
                        <Link class="nav-link" to={"/home"}>NEXT</Link>
                    </li>
                </ul>
                <div className="login-wrapper container-fluid">
                    <form>
                        <div class="mb-3 username-wrapper">
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Username" />
                        </div>
                        <div class="mb-3 password-wrapper">
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" />
                        </div>
                        <div class="mb-3 forgot-password">
                            <label class="forgot-password-label">Forgot something? <span>Reset your password</span></label>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}
