import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import logo from './assets/img/90677.jpg'

export default class Home extends Component {
    render() {
        return (
            <div className="home-container">
                <ul class="nav header-nav">
                    <li class="nav-item back">
                        <Link class="nav-link" to={"/home"}><i class="fab fa-phoenix-squadron"></i></Link>
                    </li>
                    <li class="nav-item next">
                        <Link class="nav-link" to={"/home"}><i class="far fa-bell"></i></Link>
                    </li>
                </ul>
                <div className="home-wrapper container-fluid">
                    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"></li>
                            <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"></li>
                            <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src={logo} class="d-block w-100" alt="..." />
                            </div>
                            <div class="carousel-item">
                                <img src={logo} class="d-block w-100" alt="..." />
                            </div>
                            <div class="carousel-item">
                                <img src={logo} class="d-block w-100" alt="..." />
                            </div>
                        </div>
                    </div>
                    <div class="card text-center">
                        <div class="card-header">
                            <ul class="nav header-card">
                                <li class="nav-item user">
                                    <img src={logo} class="user-img" />
                                    <span>
                                        <div>
                                            <p>Alpa</p>
                                            <p><i class="fas fa-map-marker-alt"></i> Indonesia</p>
                                        </div>
                                    </span>
                                </li>
                                <li class="nav-item option">
                                    <i class="fas fa-ellipsis-v"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <img src={logo} class="card-img-top feed-img" alt="..." />
                            <div class="alert alert-danger" role="alert">
                                <i class="fas fa-car"></i> Traffic Jam Report | Waiting
                            </div>
                            <div class="alert alert-light" role="alert">
                                <i class="far fa-hand-rock"></i> <i class="far fa-comment"></i>
                            </div>
                            <p class="support-text">0 Supoort</p>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <p class="card-comment">
                                <span>CallCenter : </span>
                                With supporting text below as a natural lead-in to additional content.
                            </p>
                        </div>
                    </div>
                    <div class="card text-center">
                        <div class="card-header">
                            <ul class="nav header-card">
                                <li class="nav-item user">
                                    <img src={logo} class="user-img" />
                                    <span>
                                        <div>
                                            <p>Alpa</p>
                                            <p><i class="fas fa-map-marker-alt"></i> Indonesia</p>
                                        </div>
                                    </span>
                                </li>
                                <li class="nav-item option">
                                    <i class="fas fa-ellipsis-v"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <img src={logo} class="card-img-top feed-img" alt="..." />
                            <div class="alert alert-danger" role="alert">
                                <i class="fas fa-car"></i> Traffic Jam Report | Waiting
                            </div>
                            <div class="alert alert-light" role="alert">
                                <i class="far fa-hand-rock"></i> <i class="far fa-comment"></i>
                            </div>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <p class="card-comment">
                                <span>CallCenter : </span>
                                With supporting text below as a natural lead-in to additional content.
                            </p>
                        </div>
                    </div>
                </div>
                <ul class="nav footer-nav nav-fill">
                    <li class="nav-item">
                        <Link class="nav-link active" to={"/home"}>
                            <i class="fas fa-home"></i>
                            <p>Home</p>
                        </Link>
                    </li>
                    <li class="nav-item">
                        <Link class="nav-link">
                            <i class="far fa-comments"></i>
                            <p>Chat</p>
                        </Link>
                    </li>
                    <li class="nav-item">
                        <Link class="nav-link button-report" to={"/report-photo"}>
                            <i class="fas fa-plus-circle"></i>
                            <p>Report</p>
                        </Link>
                    </li>
                    <li class="nav-item">
                        <Link class="nav-link">
                            <i class="fas fa-globe-asia"></i>
                            <p>Search</p>
                        </Link>
                    </li>
                    <li class="nav-item">
                        <Link class="nav-link">
                            <i class="far fa-user-circle"></i>
                            <p>Profile</p>
                        </Link>
                    </li>
                </ul>
            </div>
        )
    }
}
