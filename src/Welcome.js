import React, { Component } from 'react'

import { Link } from 'react-router-dom'
import logo from './assets/img/708.png'

export default class Welcome extends Component {
    render() {
        return (
            <div className="welcome-container container-fluid">
                <img className="welcome-logo" src={logo} />
                <h5 className="welcome-headline">Welcome to Qlue</h5>
                <p className="welcome-sub-headline">Where you can contribute in fixing your cities by reporting problems to the city administration. Please Login or Register below!</p>
                <ul className="list-group list-group-flush welcome-list-group">
                    <li className="list-group-item">
                        <button type="button" className="btn btn-outline-primary">
                            <div className="row">
                                <div className="col-2">
                                    <i className="fas fa-user btn-icon"></i>
                                </div>
                                <div className="col-8">
                                    <Link to={"/login"}>Login With Username</Link>
                                </div>
                            </div>
                        </button>
                    </li>
                    <li className="list-group-item">
                        <button type="button" className="btn btn-outline-primary">
                            <div className="row">
                                <div className="col-2">
                                    <i class="fab fa-facebook-f btn-icon"></i>
                                </div>
                                <div className="col-8">
                                    <Link to={"/login"}>Login With Facebook</Link>
                                </div>
                            </div>
                        </button>
                    </li>
                    <li className="list-group-item">
                        <button type="button" className="btn btn-outline-primary">
                            <div className="row">
                                <div className="col-2">
                                    <i class="fab fa-google btn-icon"></i>
                                </div>
                                <div className="col-8">
                                    <Link to={"/login"}>Login With Google</Link>
                                </div>
                            </div>
                        </button>
                    </li>
                </ul>
                <p className="footer-welcome">Don't have an account yet? <span className="create">Create One</span></p>
            </div>
        )
    }
}
